use std::time::Duration;

use matrix_sdk::{
	config::SyncSettings,
	ruma::{events::room::message::SyncRoomMessageEvent, UserId},
	Client,
};

use matrix_sdk::room::Room;
use matrix_sdk::ruma::events::room::member::RoomMemberEventContent;
use matrix_sdk::ruma::events::{room::message::*, *};

use regex::Regex;
use tokio::fs::File;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use async_minecraft_ping::ConnectionConfig;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
	let userid = std::env::var("USERID")?;
	let pass = std::env::var("PASSWORD")?;

	let u = <&UserId>::try_from(<String as AsRef<str>>::as_ref(&userid))?;
	let client = Client::builder()
		.sled_store("store", None)?
		.server_name(u.server_name())
		.build()
		.await?;

	// First we need to log in.
	let login_builder = client.login_username(u, &pass);

	if let Ok(mut f) = File::open("device_id").await {
		let mut device_id_str = String::new();
		f.read_to_string(&mut device_id_str).await?;

		login_builder.device_id(&device_id_str).send().await?;
	} else {
		let response = login_builder.send().await?;
		let mut f = File::create("device_id").await?;
		f.write_all(response.device_id.as_bytes()).await?;
	}

	client.add_event_handler(handle_message_event);

	client.add_event_handler(
		|ev: StrippedStateEvent<RoomMemberEventContent>, r: Room, client: Client| async move {
			println!( "Received a StrippedStateEvent<RoomMemberEventContent> {:#?}, {:#?}", ev, r);
			if let Room::Invited(inner) = r {
					let _ = client.join_room_by_id(inner.room_id()).await;
			}
		},
	);

	let enc = client.encryption();
	println!("{:#?}", enc.cross_signing_status().await);

	for room in client.invited_rooms() {
		client.join_room_by_id(room.room_id()).await;
	}

	// Syncing is important to synchronize the client state with the server.
	// This method will never return.
	client.sync(SyncSettings::default()).await?;

	Ok(())
}

async fn handle_message_event(
	ev: SyncRoomMessageEvent,
	r: Room,
	_client: Client,
) {
	println!("Received a message {:?}", ev);
	if let SyncMessageLikeEvent::Original(o) = ev {
		if let (MessageType::Text(t), Room::Joined(room)) =
			(o.content.msgtype, r)
		{
			let mut args = t.body.split_whitespace();
			if args.next().unwrap() == "!check" {
				let text = match args.next() {
					Some(addr) => {
						let reg = Regex::new(r#"^(.*?)(?::(\d+))?$"#).unwrap();
						if let Some(captures) = reg.captures(addr) {
							let addr = captures.get(1).unwrap().as_str();
							let port = captures
								.get(2)
								.and_then(|x| x.as_str().parse::<u16>().ok())
								.unwrap_or(25565);

							let config = ConnectionConfig::build(addr)
								.with_timeout(Duration::from_secs(1))
								.with_port(port);

							match config.connect().await {
								Ok(connection) => {
									match connection.status().await {
										Ok(status) => {
											format!(
												"Server online: {}/{} players",
												status.status.players.online,
												status.status.players.max
											)
										}
										Err(err) => err.to_string(),
									}
								}
								Err(err) => err.to_string(),
							}
						} else {
							"invalid ip or port".to_string()
						}
					}
					None => "missing ip:port".to_string(),
				};
				let _ = room
					.send(RoomMessageEventContent::text_plain(text), None)
					.await;
			}
		};
	}
}
